package com.ivan.beans;

public class ImportParameters {
	String pathCsvFileImport;
	String pathJsonFileOutput;
	String pathCsvErrorFile;
	
	public String getPathCsvFileImport() {
		return pathCsvFileImport;
	}
	
	public void setPathCsvFileImport(String pathCsvFileImport) {
		this.pathCsvFileImport = pathCsvFileImport;
	}
	
	public String getPathCsvErrorFile() {
		return pathCsvErrorFile;
	}
	
	public void setPathCsvErrorFile(String pathCsvErrorFile) {
		this.pathCsvErrorFile = pathCsvErrorFile;
	}

	public String getPathJsonFileOutput() {
		return pathJsonFileOutput;
	}

	public void setPathJsonFileOutput(String pathJsonFileOutput) {
		this.pathJsonFileOutput = pathJsonFileOutput;
	}

}
