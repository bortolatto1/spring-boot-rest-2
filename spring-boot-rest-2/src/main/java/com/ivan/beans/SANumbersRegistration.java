package com.ivan.beans;

import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import org.springframework.beans.factory.annotation.Autowired;

public class SANumbersRegistration {	
	
	private static final int LENGHTNUMBER = 11;
	
	@Autowired
	private String errorMessage;
    private List<SANumbers> SANumbersRecords;
    private static SANumbersRegistration stdregd = null;
    private static ImportParameters importParameters = null;
    
    private SANumbersRegistration(){
    	SANumbersRecords = new ArrayList<SANumbers>();

    	if (this.fileExists(importParameters.getPathJsonFileOutput())) {
    		SANumbersRecords = this.readFileJson(importParameters.getPathJsonFileOutput());
    	}	
    }
    
    public static SANumbersRegistration getInstance(ImportParameters myImportParameters) {
    	importParameters = myImportParameters;
        if(stdregd == null) {
        	stdregd = new SANumbersRegistration();
            return stdregd;
        }
        else {
        	return stdregd;
        }
    }
    
    public SANumbersReply add(SANumbers std) {
    	SANumbersReply result = new SANumbersReply();      	
		
		long numberPhone = correctNumber(std.getPhone());
		if (numberPhone == -1) {	
			numberPhone = Long.parseLong(std.getPhone().replaceAll("[^\\d.]", ""));
			result.setId(std.getId());	
			result.setPhone(numberPhone);
			result.setRegistrationStatus(errorMessage);
		}
		else {
			std.setPhone(Long.toString(numberPhone));				
			SANumbersRecords.add(std);
			result.setId(std.getId());	
			result.setPhone(numberPhone);
			result.setRegistrationStatus("Successful");			

	    	//Cancellazione preventiva del file, per poi ricrearlo
	    	if (this.fileExists(importParameters.getPathJsonFileOutput())) {
	    		this.fileDelete(importParameters.getPathJsonFileOutput());
	    	}	
	    	this.writeFileJson(importParameters.getPathJsonFileOutput());
		}	    	
		errorMessage = "";
    	return result;
    }
    
    public SANumbersReply upDateSANumber(SANumbers std) {
    	SANumbersReply result = new SANumbersReply();   	

    	int index = searchId(std.getId());
    	if (index != -1) {    		
    		long numberPhone = correctNumber(std.getPhone());
    		if (numberPhone == -1) {   
    			numberPhone = Long.parseLong(std.getPhone().replaceAll("[^\\d.]", ""));
    			result.setId(std.getId());	
    			result.setPhone(numberPhone);
    			result.setRegistrationStatus(errorMessage);
    		}
    		else {
    			std.setPhone(Long.toString(numberPhone));				
    			SANumbersRecords.set(index, std);
    			result.setId(std.getId());	
    			result.setPhone(numberPhone);
    			result.setRegistrationStatus("Successful");			

        		//Cancellazione preventiva del file, per poi ricrearlo
        		if (this.fileExists(importParameters.getPathJsonFileOutput())) {
        	    	this.fileDelete(importParameters.getPathJsonFileOutput());
        		}
        		this.writeFileJson(importParameters.getPathJsonFileOutput()); 
    		}
    	} 		
		errorMessage = "";
    	return result;
    }
    
    public String deleteSANumber(String myId) {
    	
    	for(int i=0; i<SANumbersRecords.size(); i++) {
    		SANumbers stdn = SANumbersRecords.get(i);
    		if(stdn.getId().equals(myId)) {
    			SANumbersRecords.remove(i);//update the new record
    			
    	    	//Cancellazione preventiva del file, per poi ricrearlo
    	    	if (this.fileExists(importParameters.getPathJsonFileOutput())) {
    	    		this.fileDelete(importParameters.getPathJsonFileOutput());
    	    	}	
    	    	this.writeFileJson(importParameters.getPathJsonFileOutput());  
    			
    	        return "Delete successful";
    		}
    	}
    	return "Delete un-successful";
    }

    public List<SANumbers> getSANumbersRecords() {
  	    return SANumbersRecords;
    }    
    
    public List<SANumbers> importSANumber() {
    	List<SANumbers> result = new ArrayList<SANumbers>();

    	this.writeCsvErrorHeader(importParameters.getPathCsvErrorFile());
    	boolean intestazione = true;    	
    	
    	if (this.fileExists(importParameters.getPathCsvFileImport())) {
    		String row = "";
    		try {
    			BufferedReader csvReader = new BufferedReader(new FileReader(importParameters.getPathCsvFileImport()));
				while ((row = csvReader.readLine()) != null) {
					if (intestazione) {
						intestazione = false;
						continue;
					}
				    String[] data = row.split(",");//data[0] = id; data [1] = number
				    
					SANumbers myNumber = new SANumbers();
					myNumber.setId(data[0]);
					myNumber.setPhone(data[1]);
					
					long numberPhone = correctNumber(myNumber.getPhone());
					if (numberPhone == -1) {	
						//scrittura file csv errore
						this.writeCsvError(importParameters.getPathCsvErrorFile(), myNumber.getId(), myNumber.getPhone());
						continue;
					}
					else {
						myNumber.setPhone(Long.toString(numberPhone));
					}	
					
					int index = searchId(myNumber.getId());
					if (index == -1) {  //numero nuovo
					    SANumbersRecords.add(myNumber);
					}
					else {    	    		
						SANumbersRecords.set(index, myNumber);
					} 	
				}
				csvReader.close(); 
			} catch (IOException e) {
				e.printStackTrace();
			}  		
    		
    		//Cancellazione preventiva del file, per poi ricrearlo
    		if (this.fileExists(importParameters.getPathJsonFileOutput())) {
    	    	this.fileDelete(importParameters.getPathJsonFileOutput());
    	    }	
    	    this.writeFileJson(importParameters.getPathJsonFileOutput()); 
    	    
    	    result = SANumbersRecords;
    	}  	
    	
    	return result;
    }
    
    private boolean fileExists(String completeNameFile) {
    	boolean result;
    	
    	Path myPathFile = Paths.get(completeNameFile);
    	result = Files.exists(myPathFile);
    	
    	return result;
    }
    
    private void fileDelete(String completeNameFile) {
    	Path myPathFile = Paths.get(completeNameFile);
    	try {
			Files.delete(myPathFile);
		} catch (IOException e) {		}
    }
    
    private List<SANumbers> readFileJson(String completeNameFile) {
    	List<SANumbers> result = new ArrayList<SANumbers>();
    	
    	final Type SANUMBER_TYPE = new TypeToken<List<SANumbers>>() { 	}.getType();
    	Gson gson = new Gson();
    	JsonReader reader = null;
		try {
			reader = new JsonReader(new FileReader(completeNameFile));
			result = gson.fromJson(reader, SANUMBER_TYPE); // contains the whole reviews list
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		
		try {
			reader.close();
		} catch (IOException e) {}
		
    	return result;
    }    
    
    private boolean writeFileJson(String completeNameFile) {
    	//boolean ritorna true se tutto ok, altrimenti false
    	boolean result = true;
    	Gson gson = new Gson();
    	
    	try (Writer writer = new FileWriter(completeNameFile)) {
    	    gson = new GsonBuilder().setPrettyPrinting().create();
    	    gson.toJson(SANumbersRecords, writer);
    	    writer.close();
    	} catch (IOException e) {
    		result = false;
    	}
    	return result;
    }    
    
    private int searchId(String inputId) {
    	int result = -1;
    	
    	for(int i=0; i<SANumbersRecords.size(); i++) {
    		SANumbers stdn = SANumbersRecords.get(i);
    	    if(stdn.getId().equals(inputId)) {
    	    	result = i;
    	    }
        }    	    	
    	return result;
    }    
    
    private long correctNumber(String numberPhone) {
    	long result = -1;
    	errorMessage = "";
    	
    	//tentativi di correggere il numero
       	numberPhone = numberPhone.replaceAll("[^\\d.]", "");
       	
       	if (numberPhone.length() > LENGHTNUMBER) {
       		errorMessage = "number too long";
       	} else if (numberPhone.length() < LENGHTNUMBER) {
       		errorMessage = "number too short";
       	} else {
       		errorMessage = "";
       		result = Long.parseLong(numberPhone);
       	}
    	
    	return result;
    }    
    
    private void writeCsvErrorHeader(String completeNameFileError) {    	
    	
    	//Cancellazione preventiva del file errori, per ricrearlo 
    	if (this.fileExists(completeNameFileError)) {
    		this.fileDelete(completeNameFileError);
    	}	

		try {
			Writer writer = new FileWriter(completeNameFileError);
	    	StringBuilder line = new StringBuilder();
	    	line.append("\"id\",\"sms_phone\",\"Error\"\n");            
	    	writer.write(line.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }  
    
    private boolean writeCsvError(String completeNameFileError, String phoneId, String phoneNumber) {
    	//boolean ritorna true se tutto ok, altrimenti false
    	boolean result = true;
    	
    	try (Writer writer = new FileWriter(completeNameFileError, true)) {
    		StringBuilder line = new StringBuilder();
    		line.append("\"");
    		line.append(phoneId.replaceAll("\"","\"\""));
    		line.append("\",\"");
    		line.append(phoneNumber.replaceAll("\"","\"\""));
    		line.append("\",\"");
    		line.append(errorMessage);
    		line.append("\"\n");
    		writer.write(line.toString());
    		writer.close();
    	} catch (IOException e) {
    		result = false;
    	}   
    	return result;
    }    
    
}
