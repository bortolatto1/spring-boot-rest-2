package com.ivan.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ivan.beans.*;

@Controller
public class SANumberRegistrationController {
	
	@Value("${pathCsvFileImport.path}") private String pathCsvFileImport;
	@Value("${pathCsvErrorFile.path}") private String pathCsvErrorFile;
	@Value("${pathJsonFileOutput.path}") private String pathJsonFileOutput;
	
	@RequestMapping(method = RequestMethod.POST, value="/register/sanumber")
	
	@ResponseBody
	public SANumbersReply registerSANumber(@RequestBody SANumbers sanumbers) {
		
		ImportParameters importParameters = new ImportParameters();
		importParameters.setPathCsvFileImport(pathCsvFileImport);
		importParameters.setPathCsvErrorFile(pathCsvErrorFile);
		importParameters.setPathJsonFileOutput(pathJsonFileOutput);		
		
		SANumbersReply stdregreply = SANumbersRegistration.getInstance(importParameters).add(sanumbers);
		
        return stdregreply;
	}
}
