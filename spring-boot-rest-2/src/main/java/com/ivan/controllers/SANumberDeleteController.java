package com.ivan.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;

import com.ivan.beans.ImportParameters;
import com.ivan.beans.SANumbersRegistration;

@Controller
public class SANumberDeleteController {
	
	@Value("${pathCsvFileImport.path}") private String pathCsvFileImport;
	@Value("${pathCsvErrorFile.path}") private String pathCsvErrorFile;
	@Value("${pathJsonFileOutput.path}") private String pathJsonFileOutput;
	
	@RequestMapping(method = RequestMethod.DELETE, value="/delete/sanumber/{regdNum}")

	@ResponseBody
	public String deleteSANumberRecord(@PathVariable("regdNum") String regdNum) {
		System.out.println("In deleteSANumberRecord");   
		
		ImportParameters importParameters = new ImportParameters();
		importParameters.setPathCsvFileImport(pathCsvFileImport);
		importParameters.setPathCsvErrorFile(pathCsvErrorFile);
		importParameters.setPathJsonFileOutput(pathJsonFileOutput);	
		
	    return SANumbersRegistration.getInstance(importParameters).deleteSANumber(regdNum);
	}
}
