package com.ivan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ivan.beans.*;

@Controller
public class SANumberImportController {
	
	@Value("${pathCsvFileImport.path}") private String pathCsvFileImport;
	@Value("${pathCsvErrorFile.path}") private String pathCsvErrorFile;
	@Value("${pathJsonFileOutput.path}") private String pathJsonFileOutput;

	@RequestMapping(method = RequestMethod.GET, value="/import/sanumber")
	
	@ResponseBody
	public List<SANumbers> importSANumber() {
		
		ImportParameters importParameters = new ImportParameters();
		importParameters.setPathCsvFileImport(pathCsvFileImport);
		importParameters.setPathCsvErrorFile(pathCsvErrorFile);
		importParameters.setPathJsonFileOutput(pathJsonFileOutput);		
		
	    return SANumbersRegistration.getInstance(importParameters).importSANumber();
	}
	
}
