package com.ivan.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ivan.beans.ImportParameters;
import com.ivan.beans.SANumbers;
import com.ivan.beans.SANumbersRegistration;
import com.ivan.beans.SANumbersReply;

@Controller
public class SANumberUpdateController {
	
	@Value("${pathCsvFileImport.path}") private String pathCsvFileImport;
	@Value("${pathCsvErrorFile.path}") private String pathCsvErrorFile;
	@Value("${pathJsonFileOutput.path}") private String pathJsonFileOutput;
	
	@RequestMapping(method = RequestMethod.PUT, value="/update/sanumber")

	@ResponseBody
	public SANumbersReply updateSANumberRecord(@RequestBody SANumbers stdn) {
		System.out.println("In updateSANumberRecord");   
		
		ImportParameters importParameters = new ImportParameters();
		importParameters.setPathCsvFileImport(pathCsvFileImport);
		importParameters.setPathCsvErrorFile(pathCsvErrorFile);
		importParameters.setPathJsonFileOutput(pathJsonFileOutput);	
		
		SANumbersReply stdregreply =  SANumbersRegistration.getInstance(importParameters).upDateSANumber(stdn);		
        return stdregreply;   
	}

}
